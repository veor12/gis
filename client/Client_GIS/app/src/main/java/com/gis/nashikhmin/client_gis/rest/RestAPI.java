package com.gis.nashikhmin.client_gis.rest;

import com.gis.nashikhmin.client_gis.model.Greeting;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by nashikhmin on 12.03.16.
 */
public class RestAPI {
    private static String url =  "http://rest-service.guides.spring.io/greeting";

    public static Greeting get() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Greeting greeting = restTemplate.getForObject(url, Greeting.class);
        return greeting;
    }
}
