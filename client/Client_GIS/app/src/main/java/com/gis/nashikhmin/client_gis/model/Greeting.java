package com.gis.nashikhmin.client_gis.model;


public class Greeting {
    private String id;
    private String content;

    public String getContent() {
        return content;
    }

    public String getId() {
        return id;
    }
}
