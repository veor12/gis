from rest_framework import serializers

from routers_service.models import Route


class RouteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Route
        fields = ('id','title','owner', 'points')