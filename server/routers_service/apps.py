from django.apps import AppConfig


class RoutersServiceConfig(AppConfig):
    name = 'routers_service'
