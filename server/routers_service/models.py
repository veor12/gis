from django.db import models

# Create your models here.
class Route(models.Model):
    title = models.CharField(max_length=100, blank=True, default='', null=False)
    owner = models.CharField(max_length=100)
    points = models.TextField()

    class Meta:
        ordering = ('title',)